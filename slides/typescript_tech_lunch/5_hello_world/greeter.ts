class Greeter {
	constructor( public name: string ) {
	}
	public greet(): void {
		var greeting: string = "Hello " + this.name + "!";
		document.body.innerHTML = greeting;
	}
}
var greeter = new Greeter( "Tech Lunch" );
greeter.greet();
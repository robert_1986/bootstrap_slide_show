var Greeter = (function () {
    function Greeter(name) {
        this.name = name;
    }
    Greeter.prototype.greet = function () {
        var greeting = "Hello " + this.name + "!";
        document.body.innerHTML = greeting;
    };
    return Greeter;
})();
var greeter = new Greeter("Tech Lunch");
greeter.greet();
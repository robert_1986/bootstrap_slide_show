/// <reference path="typings/jquery.d.ts" />

$(() => {

	$("#hideButton").click((e) => {
		$('#colorPanel').hide(1000);
	});

	$("#showButton").click((e) => {
		$('#colorPanel').show(1000);
	});

});

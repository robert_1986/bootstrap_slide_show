/// <reference path="typings/jquery.d.ts" />
$(function () {
    $("#hideButton").click(function (e) {
        $('#colorPanel').hide(1000);
    });
    $("#showButton").click(function (e) {
        $('#colorPanel').show(1000);
    });
});

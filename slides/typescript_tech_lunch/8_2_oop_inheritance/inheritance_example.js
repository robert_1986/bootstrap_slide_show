var __extends = this.__extends || function (d, b) {
	for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	function __() { this.constructor = d; }
	__.prototype = b.prototype;
	d.prototype = new __();
};

var Shape = (function () {
	function Shape() {
		this.location = new Point(0, 0);
	}
	Shape.prototype.move = function (newLocation) {
		this.location = newLocation;
	};
	return Shape;
})();

var Rectangle = (function (_super) {
	__extends(Rectangle, _super);
	function Rectangle() {
		_super.call(this);
		this.height = 0;
		this.width = 0;
	}
	Rectangle.prototype.area = function () {
		return this.height * this.width;
	};
	return Rectangle;
})(Shape);
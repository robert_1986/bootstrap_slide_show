class Shape {
	public location: IPoint = new Point(0, 0);
	constructor() {
	}
	public move(newLocation: IPoint) {
		this.location = newLocation;
	}
}

class Rectangle extends Shape {
	public height: number = 0;
	public width: number = 0;
	constructor() {
		super();
	}
	public area(): number {
		return this.height * this.width;
	}
}
# Bootstrap Slide Show Setup #

## Version 1.0 ##
### 2015-08-27 ###

1. git clone https://robert_1986@bitbucket.org/robert_1986/bootstrap_slide_show.git

2. cd to project directory

3. Run bower install to install dependancies

4. Building your presentation. Create a folder in slides/ to hold your slide html files. Refer to slides/demo-set/ for example. Bootstrap classes are used to style the slides content. By deafult the pb-design-system styles are applied.

5. Configure the presentation you want to use. In index.html the Presentation.init() function call sets up the presentation loading the slides in a defined order. For this to work you need to pass some parameters to the function;

```
#!javascript

Presentation.init( <slide_container> <slides_folder> <slide_names_array> [use_buttons]);
```
*	slide_container: jQuery selector where the slides should be loaded ( $('#slides') )
* 	slides_folder: string representing the naem of the folder where the html slides are stored within the project root/slides.direcotry
* 	slide_names_array: array containg and ordered list of slide names that defines their order within the presentation. This array is used to load the slides and the names should be the html file names without the .html file extension.
* 	use_buttons: optional boolean that defines if the control bar is shown in the bottom right 

6) The presentation requires the slide html files to be served via http, the simplest solution to this is to use node.js and the http-server package. Ff not installed this can be installed via npm. To start the server run http-server from the root dir of this project.

# Controls #
Use the left and right arrow keys to move between the slides or enter a number in the test box and hit Go! to jump to that slide.

# Branches #
* master - this branch is configured to use a default demo set of slides for example purposes
* typescript_tech_lunch - this branch is configured to use the slides from the typescript tech lunch presented on 2015-08-27
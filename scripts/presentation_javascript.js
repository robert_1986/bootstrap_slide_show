var Presentation = {

	total_slides: 0,

	current_slide: 1,

	slide_container: null,

	current_offset: 0,

	resize_timer_function: null,

	use_buttons: false,

	mouse_active: false,

	init: function( container, directory, slides_array, use_buttons ) {

		if( !slides_array ) {
			return false;
		}

		if( use_buttons ) {
			Presentation.use_buttons = true;
		}
		else {
			$( "#controlbar" ).remove();
		}

		Presentation.slide_container = container;
		Presentation.total_slides = slides_array.length;
		Presentation.load_content( directory, slides_array );
		Presentation.key_press();
		Presentation.controls_bar();
		Presentation.resize_detect();

		var hashNumber = parseInt( window.location.hash.replace( "#", "" ) );
		if( !isNaN( hashNumber ) ) {
			Presentation.goto_slide( hashNumber );
		}

	},

	load_content: function( directory, slides_array ) {
		Presentation.slide_container.hide();
		$.each( slides_array, function( index, value ) {
			$( "<div id=\"slide-" + ( index + 1 ) + "\"></div>" )
				.load( "slides/" + directory + "/" + value + ".html" )
				.addClass( "container" )
				.appendTo( Presentation.slide_container );
		} );
		Presentation.slide_container.show();
		Presentation.vertially_center();
	},

	vertially_center: function() {
		var window_height = window.innerHeight;
		var slide_height = $( "#slides div:eq(0)" ).outerHeight();

		if( slide_height >= window_height ) {
			Presentation.slide_container.css( "margin-top", "none" );
		}
		else {
			var margin_top = ( window_height - slide_height ) / 2;
			Presentation.slide_container.css( "margin-top", margin_top + "px" );
		}
	},

	key_press : function() {
		$( document.body ).keydown( function( e ) {
			// if left (37) or right (39) arrow key is pressed
			if ( e.keyCode === 39 || e.keyCode === 37 ) {
				e.preventDefault();
				( e.keyCode === 39 ) ? Presentation.next_slide() : Presentation.prev_slide();
			}
		});
	},

	mouse_click: function () {
		$( document.body ).on( "mousedown", function( e ) {
			if( e.which == 1 || e.which == 2 || e.which == 3 ) {
				e.preventDefault();
				( e.which === 1 ) ? Presentation.next_slide() : Presentation.prev_slide();
			}
		});
	},

	controls_bar: function() {
		if( Presentation.use_buttons ) {

			$( "#next_slide" ).on( "click", function( e ) {
				e.preventDefault();
				Presentation.next_slide();
			});

			$( "#prev_slide" ).on( "click", function( e ) {
				e.preventDefault();
				Presentation.prev_slide();
			});

			$( "#goto" ).on( "click", function( e ) {
				e.preventDefault();
				var num = $( "#goto_number" ).val();
				Presentation.goto_slide( num );
			});

			$( "#goto_number" ).attr( "max", Presentation.total_slides );

			$( "#goto_form" ).on( "submit", function( e ) {
				e.preventDefault();
				var num = $( "#goto_number" ).val();
				Presentation.goto_slide( num );
			});

			// $( "#usemouse" ).on( "click", function( e ) {
			// 	e.preventDefault();
			// 	if( Presentation.mouse_active ) {
			// 		$( document.body ).off( "mousedown" );
			// 		$( "body" ).off( "contextmenu" );
			// 	}
			// 	else {
			// 		Presentation.mouse_click();
			// 		$( "body" ).on( "contextmenu", function() {
			// 			return false;
			// 		});
			// 	}
			// 	Presentation.mouse_active = !Presentation.mouse_active;
			// });
		}
		else {
			if( $( "#button_controls" ).length ) {
				$( "#next_slide" ).off( "click" );
				$( "#button_controls" ).hide();
			}
		}
	},

	next_slide: function() {
		if( Presentation.current_slide == Presentation.total_slides ) {
			return false;
		}

		Presentation.current_offset -= $( "#slide-" + Presentation.current_slide ).outerWidth();
		Presentation.animate_slide( "next" );
		Presentation.current_slide++;
		Presentation.update_slide_numbers();
	},

	prev_slide: function() {

		if( Presentation.current_slide <= 1 ) {
			return false;
		}

		Presentation.current_offset += $( "#slide-" + Presentation.current_slide ).outerWidth();
		Presentation.animate_slide( "prev" );
		Presentation.current_slide--;
		Presentation.update_slide_numbers();
	},

	animate_slide: function( direction ) {
		Presentation.slide_container.css( "margin-left", Presentation.current_offset + "px" );
		var next_slide_num = direction == "next" ? Presentation.current_slide + 1 : Presentation.current_slide - 1;
		$( '#slide-' + next_slide_num ).children().hide().fadeIn( 750, "easeInExpo" );
	},

	update_slide_numbers: function() {
		if( $( "#slide-" + Presentation.current_slide + " .slide-number" ).length ) {
			$( "#slide-" + Presentation.current_slide + " .slide-number" ).html( Presentation.current_slide + "/" + Presentation.total_slides );
		}
		$( "#goto_number" ).val( Presentation.current_slide );
		window.location.hash = Presentation.current_slide;
	},

	resize_detect: function() {
		$( window ).resize( function() {
			clearTimeout( Presentation.resize_timer_function );
			Presentation.resize_timer_function = setTimeout( Presentation.resize_presentation(), 250);
	    });
	},

	resize_presentation: function() {
		Presentation.vertially_center();
		Presentation.re_calc_offset();
	},

	re_calc_offset: function() {
		var offset = -( Presentation.current_slide - 1 ) * $( "#slide-" + Presentation.current_slide ).outerWidth();
		if( offset != Presentation.current_offset ) {
			Presentation.current_offset = offset;
			Presentation.slide_container.addClass( "notransition" );
			Presentation.slide_container.css( "margin-left", Presentation.current_offset + "px" );
			Presentation.slide_container[0].offsetHeight;
			Presentation.slide_container.removeClass( "notransition" );
		}
	},

	goto_slide: function( slide_number ) {

		if( slide_number >= Presentation.total_slides ) {
			Presentation.current_slide = Presentation.total_slides;
		}
		else if( slide_number <= 1 ) {
			Presentation.current_slide = 1;
		}
		else if( slide_number != Presentation.current_slide ) {
			Presentation.current_slide = slide_number;
		}
		Presentation.re_calc_offset();
		Presentation.update_slide_numbers();
	}


};